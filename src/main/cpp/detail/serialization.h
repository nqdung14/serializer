#pragma once

#include <map>
#include <set>
#include <deque>
#include <unordered_map>
#include <unordered_set>

#include <yas/types/std/pair.hpp>
#include <yas/types/std/bitset.hpp>
#include <yas/types/std/string.hpp>
#include <yas/types/std/wstring.hpp>
#include <yas/types/std/vector.hpp>
#include <yas/types/std/list.hpp>
#include <yas/types/std/forward_list.hpp>
#include <yas/types/std/array.hpp>
#include <yas/types/std/tuple.hpp>

#include <yas/detail/type_traits/type_traits.hpp>
#include <yas/detail/type_traits/serializer.hpp>

// The default yas implementations for map/set/unordered_map/unordered_set do not support custom
// Compare/Hasher, so we have to define our own versions.
//
// Also includes:
// - std::deque
// - std::vector<bool>

namespace yas {
namespace detail {

struct MapSerializer {
  template <class Ar, class M>
  static Ar &save(Ar &ar, const M &map) {
    ar.write_seq_size(map.size());
    for (const auto &it : map) {
      ar & it.first & it.second;
    }
    return ar;
  }

  template <class Ar, class M>
  static Ar &load(Ar &ar, M &map) {
    map.clear();
    size_t size = ar.read_seq_size();
    for (size_t i = 0; i < size; ++i) {
      typename M::key_type key{};
      typename M::mapped_type value{};
      ar & key & value;
      map.emplace(std::move(key), std::move(value));
    }
    return ar;
  }
};

struct SetSerializer {
  template <class Ar, class S>
  static Ar &save(Ar &ar, const S &set) {
    ar.write_seq_size(set.size());
    for (const auto &it : set) {
      ar & it;
    }
    return ar;
  }

  template <class Ar, class S>
  static Ar &load(Ar &ar, S &set) {
    set.clear();
    size_t size = ar.read_seq_size();
    for (size_t i = 0; i < size; ++i) {
      typename S::value_type value{};
      ar & value;
      set.emplace(std::move(value));
    }
    return ar;
  }
};

// map
template <std::size_t F, class K, class V, class Compare, class Allocator>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::map<K, V, Compare, Allocator>> {
  using MapType = std::map<K, V, Compare, Allocator>;

  template <class Ar>
  static Ar &save(Ar &ar, const MapType &map) {
    return MapSerializer::save(ar, map);
  }

  template <class Ar>
  static Ar &load(Ar &ar, MapType &map) {
    return MapSerializer::load(ar, map);
  }
};

// unordered_map
template <std::size_t F, class K, class V, class Hasher, class KeyEqual, class Allocator>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::unordered_map<K, V, Hasher, KeyEqual, Allocator>> {
  using MapType = std::unordered_map<K, V, Hasher, KeyEqual, Allocator>;

  template <class Ar>
  static Ar &save(Ar &ar, const MapType &map) {
    return MapSerializer::save(ar, map);
  }

  template <class Ar>
  static Ar &load(Ar &ar, MapType &map) {
    return MapSerializer::load(ar, map);
  }
};

// set
template <std::size_t F, class K, class Compare, class Allocator>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::set<K, Compare, Allocator>> {
  using SetType = std::set<K, Compare, Allocator>;

  template <class Ar>
  static Ar &save(Ar &ar, const SetType &set) {
    return SetSerializer::save(ar, set);
  }

  template <class Ar>
  static Ar &load(Ar &ar, SetType &set) {
    return SetSerializer::load(ar, set);
  }
};

// unordered_set
template <std::size_t F, class K, class Hasher, class KeyEqual, class Allocator>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::unordered_set<K, Hasher, KeyEqual, Allocator>> {
  using SetType = std::unordered_set<K, Hasher, KeyEqual, Allocator>;

  template <class Ar>
  static Ar &save(Ar &ar, const SetType &set) {
    return SetSerializer::save(ar, set);
  }

  template <class Ar>
  static Ar &load(Ar &ar, SetType &set) {
    return SetSerializer::load(ar, set);
  }
};

template <std::size_t F, class V, class Allocator>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::deque<V, Allocator>> {
  using DequeType = std::deque<V, Allocator>;

  template<class Ar>
  static Ar &save(Ar &ar, const DequeType &deque) {
    ar.write_seq_size(deque.size());
    for (const auto &it : deque) {
      ar & it;
    }
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, DequeType &deque) {
    deque.clear();
    auto size = ar.read_seq_size();
    for (; size; --size) {
      V val{};
      ar & val;
      deque.push_back(std::move(val));
    }
    return ar;
  }
};

template <std::size_t F>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::vector<bool>> {
  using Type = std::vector<bool>;

  template<class Ar>
  static Ar &save(Ar &ar, const Type &data) {
    size_t num_bytes = (data.size() + 7) / 8;
    std::vector<unsigned char> bytes(num_bytes, 0);
    for (size_t i = 0; i < data.size(); ++i) {
      bytes[i / 8] |= data[i] << (i % 8);
    }
    ar.write_seq_size(data.size());
    ar & bytes;
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, Type &data) {
    auto size = ar.read_seq_size();
    data.resize(size, true);

    std::vector<unsigned char> bytes;
    ar & bytes;
    for (size_t i = 0; i < size; ++i) {
      data[i] = (bytes[i / 8] >> (i % 8)) & 0x1;
    }
    return ar;
  }
};

}
}
