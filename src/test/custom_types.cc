#include <glog/logging.h>
#include <gtest/gtest.h>

#include <armadillo>
#include <random>
#include <stack>
#include <yas/std_streams.hpp>

#include "src/main/cpp/serializer.h"

using namespace serializer;

template <typename T>
void SaveToFile(const T &data, const std::string &filename) {
  std::ofstream ofs(filename,
                    std::ios::out | std::ios::trunc | std::ios::binary);
  yas::std_ostream yas_os(ofs);
  yas::binary_oarchive<yas::std_ostream> oa(yas_os);
  Serializer::Serialize(data, oa);
}

template <typename T>
void LoadFromFile(T &data, const std::string &filename) {
  std::ifstream ifs(filename, std::ios::in | std::ios::binary);
  yas::std_istream yas_is(ifs);
  yas::binary_iarchive<yas::std_istream> ia(yas_is);
  Serializer::Deserialize(data, ia);
}

TEST(CustomType, Stack) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 1000; ++it) {
    std::stack<int> s;
    int len = rng() % 200;
    for (int i = 0; i < len; i++) {
      int value = rng() % 1000 - 500;
      s.push(value);
    }
    SaveToFile(s, "test.out");
    std::stack<int> t;
    LoadFromFile(t, "test.out");
    ASSERT_EQ(s.size(), t.size());
    while (!s.empty()) {
      auto ss = s.top();
      s.pop();
      auto tt = t.top();
      t.pop();
      ASSERT_EQ(ss, tt);
    }
  }
}

TEST(CustomType, ArmadilloMat) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 1000; it++) {
    int n_rows = rng() % 100 + 1;
    int n_cols = rng() % 100 + 1;
    arma::Mat<int> X(n_rows, n_cols);
    for (auto &u : X) u = rng() % 1000 - 500;
    SaveToFile(X, "test.out");
    arma::Mat<int> Y;
    LoadFromFile(Y, "test.out");
    ASSERT_EQ(n_rows, Y.n_rows);
    ASSERT_EQ(n_cols, Y.n_cols);
    for (int i = 0; i < n_rows; i++) {
      for (int j = 0; j < n_cols; j++) {
        ASSERT_EQ(X(i, j), Y(i, j));
      }
    }
  }
}

TEST(CustomType, ArmadilloCol) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 1000; it++) {
    int n_rows = rng() % 10000 + 1;
    arma::Col<int> X(n_rows);
    for (auto &u : X) u = rng() % 1000 - 500;
    SaveToFile(X, "test.out");
    arma::Col<int> Y;
    LoadFromFile(Y, "test.out");
    ASSERT_EQ(n_rows, Y.n_rows);
    for (int i = 0; i < n_rows; i++) {
      ASSERT_EQ(X(i), Y(i));
    }
  }
}

TEST(CustomType, ArmadilloRow) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 1000; it++) {
    int n_cols = rng() % 10000 + 1;
    arma::Row<int> X(n_cols);
    for (auto &u : X) u = rng() % 1000 - 500;
    SaveToFile(X, "test.out");
    arma::Row<int> Y;
    LoadFromFile(Y, "test.out");
    ASSERT_EQ(n_cols, Y.n_cols);
    for (int i = 0; i < n_cols; i++) {
      ASSERT_EQ(X(i), Y(i));
    }
  }
}

TEST(CustomType, ArmadilloCube) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 1000; it++) {
    int n_cols = rng() % 50 + 1;
    int n_rows = rng() % 50 + 1;
    int n_slices = rng() % 50 + 1;
    arma::Cube<int> X(n_rows, n_cols, n_slices);
    for (auto &u : X) u = rng() % 1000 - 500;
    SaveToFile(X, "test.out");
    arma::Cube<int> Y;
    LoadFromFile(Y, "test.out");
    ASSERT_EQ(n_rows, Y.n_rows);
    ASSERT_EQ(n_cols, Y.n_cols);
    ASSERT_EQ(n_slices, Y.n_slices);
    for (int i = 0; i < n_rows; i++) {
      for (int j = 0; j < n_cols; j++) {
        for (int k = 0; k < n_slices; k++) {
          ASSERT_EQ(X(i, j, k), Y(i, j, k));
        }
      }
    }
  }
}